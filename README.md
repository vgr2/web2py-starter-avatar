# web2py-starter-avatar
Adds ability to allow user to switch avatar images to existing apps based on the web2py-starter app, which is based on the AdminLTE open source theme and template.

https://almsaeedstudio.com/preview

# Installation
- Download and extract contents to your web2py app folder usually found here: ~/web2py/applications/app/
- Make the following changes to your 'views/layout.html' file

Add this line just above the closing head tags, but under any existing css references:
    <link rel="stylesheet" href="{{=URL('static','css/avatar.css')}}">

Just above this line:
    <!-- User Account Menu -->
add these lines:
    <!-- Load avatar data -->
    {{include '../views/avatar/avatar_load_data.html'}}"

Just under this line:
    <!-- The user image in the navbar-->
change this line:
    <img src="{{=URL('static','img/avatar5.png')}}" class="user-image" alt="User Image">
to this:
    {{include '../views/avatar/avatar_navbar_img.html'}}

Just under these lines:
    <!-- User Menu Header -->
    <li class="user-header">
change this line:
    <img src="{{=URL('static','img/avatar5.png')}}" class="user-image" alt="User Image">
to this:
    {{include '../views/avatar/avatar_img.html'}}


Directly under these lines:
    {{block center}}
      {{include}}
    {{end}}
add this line:
    {{include '../views/avatar/avatar_modal.html'}}

# TODO
- Update 'views/avatar/avatar_modal.html' to generate the links from an array of image names (currently hard coded)
- Add capacity to upload an avatar image
